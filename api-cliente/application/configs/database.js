const mysql = require('mysql');

const connection = () => {
	return mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '123',
		database: 'pps_api_cliente'
	});
}

module.exports = () => {
	return connection;
}