const path = require('path');
const crypto = require("crypto");

module.exports.cliente_get = (app, req, res) => {

	let id_cliente = req.params.id_cliente;

	let connection = app.configs.database();
	let clienteModel = new app.models.cliente_model.Cliente(connection);

	clienteModel.getCliente(id_cliente, function(error, result, fields){
		if (error) throw error;
		
		res.status(201).send(fields);
	});	
}

module.exports.cliente_post = (app, req, res) => {

	let cliente = {
		'nome': req.body.nome
	}

	let connection = app.configs.database();

	let clienteModel = new app.models.cliente_model.Cliente(connection);

	clienteModel.postCliente(cliente, function(error, result){
		if (error) throw error;
		res.status(201).send(result);
	});
}

module.exports.cliente_put = (app, req, res) => {

	let connection = app.configs.database();

	res.status(201).send('Requisição PUT recebida com sucesso!')
}

module.exports.cliente_delete = (app, req, res) => {

	let connection = app.configs.database();

	res.status(201).send('Requisição DELETE recebida com sucesso!')
}