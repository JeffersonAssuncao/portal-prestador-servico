const multer = require("multer");

module.exports = (app) => {
	app.get('/api-cliente/:id_cliente', (req, res) => {
		app.controllers.cliente_controller.cliente_get(app, req, res);
	});

	app.post('/api-cliente', multer().single('foto'), (req, res) => {
		app.controllers.cliente_controller.cliente_post(app, req, res);
	});

	app.put('/api-cliente/:id_cliente', multer().single('foto'), (req, res) => {
		app.controllers.cliente_controller.cliente_put(app, req, res);
	});

	app.delete('/api-cliente/:id_cliente', (req, res) => {
		app.controllers.cliente_controller.cliente_delete(app, req, res);
	});
}