const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const consign = require('consign');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

consign({cwd: 'application'})
    .include('routes')
    .then('controllers')
    .then('models')
    .then('configs/database.js')
    .into(app);

module.exports = app;